package com.nimrald.sevensigns.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nimrald.sevensigns.myapplication.Note_support.Notes;
import com.nimrald.sevensigns.myapplication.Note_support.NotesAdapter;
import com.nimrald.sevensigns.myapplication.URLs.URLs;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class WarrantyActivity extends AppCompatActivity {

    Button addNote, doneButton;

    String id;
    String response_details;

    TextView cat, subCat, serial, issue;
    ListView noteList;
    List noteSave = new ArrayList();

    NotesAdapter notesAdapter;

    ImageView im1, im2;
    private static final int CAMERA_PIC_REQUEST_IM1 = 1337;
    private static final int CAMERA_PIC_REQUEST_IM2 = 1338;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_warranty);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        id = getIntent().getStringExtra("ID");

        cat = findViewById(R.id.category_Warranty);
        subCat = findViewById(R.id.subCategory_Warranty);
        serial = findViewById(R.id.serialNumber_Warranty);
        issue = findViewById(R.id.issue_Warranty);
        im1 = findViewById(R.id.image1_Warranty);
        im2 = findViewById(R.id.image2_Warranty);
        doneButton = findViewById(R.id.doneButton_Warranty);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            new WarrantyActivity.HTTPAsyncTaskJobDetails().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URLs.getIssuesURL() + "/" + id);
        } else {
            new WarrantyActivity.HTTPAsyncTaskJobDetails().execute(URLs.getIssuesURL() + "/" + id);
        }


        noteList = findViewById(R.id.noteBanner_Warranty);
        notesAdapter = new NotesAdapter(this, R.layout.note_custom_layout);
        noteList.setAdapter(notesAdapter);

        addNote = findViewById(R.id.addNote_Warranty);
        addNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTextInputDialog();
            }
        });

        noteList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                confirmDeleteDialog(position);
                return false;
            }
        });

        im1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST_IM1);
            }
        });

        im2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST_IM2);
            }
        });

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i =0; i< noteSave.size();i++){
                    Log.d("NOTES", noteSave.get(i).toString());
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (requestCode == CAMERA_PIC_REQUEST_IM1 && resultCode == RESULT_OK) {
            Bitmap image = (Bitmap) data.getExtras().get("data");
            im1.setImageBitmap(image);
        }

        if (requestCode == CAMERA_PIC_REQUEST_IM2 && resultCode == RESULT_OK) {
            Bitmap image = (Bitmap) data.getExtras().get("data");
            im2.setImageBitmap(image);
        }
    }

    private void confirmDeleteDialog(final int position) {
        final int pos = position;

        if (pos > 0) {
            DialogInterface.OnClickListener okListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    notesAdapter.remove(pos);
                    notesAdapter.notifyDataSetChanged();
                    noteSave.remove(pos);
                }
            };

            com.nimrald.sevensigns.myapplication.AlertDialogManager.AlertDialog.yesNoDailog("Do you want to remove this note?", WarrantyActivity.this, okListener);
        } else {

            DialogInterface.OnClickListener okListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            };

            com.nimrald.sevensigns.myapplication.AlertDialogManager.AlertDialog.showErrorAlert(WarrantyActivity.this, "You Cannot remove initial notes.", okListener);
        }
    }


    private void showTextInputDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Notes");

        View viewInflated = getLayoutInflater().inflate(R.layout.text_input_dialog_layout, null);
        final EditText input = viewInflated.findViewById(R.id.input);

        builder.setView(viewInflated);

        builder.setPositiveButton("ADD", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String nt = input.getText().toString();
                Notes notes = new Notes(nt);
                notesAdapter.add(notes);
                notesAdapter.notifyDataSetChanged();
                noteSave.add(nt);

                //hide the keyboard
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private class HTTPAsyncTaskJobDetails extends AsyncTask<String, Void, String> {

        ProgressDialog dialog = new ProgressDialog(WarrantyActivity.this);

        @Override
        protected void onPreExecute() {

            dialog.setMessage("Please wait....");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... urls) {
            try {
                try {
                    return HttpJobsRequest(urls[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                    return "Error!";
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();
            findDetails(response_details);
        }
    }


    private String HttpJobsRequest(String myUrl) throws IOException, JSONException {

        URL url = new URL(myUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
        conn.connect();

        InputStream inputStream = new BufferedInputStream(conn.getInputStream());
        convertStreamToString(inputStream);
        return conn.getResponseMessage() + "";
    }

    private void convertStreamToString(InputStream is) {
        try {
            response_details = null;
            int n = 0;
            char[] buffer = new char[1024 * 4];
            InputStreamReader reader = new InputStreamReader(is, "UTF8");
            StringWriter writer = new StringWriter();
            while (-1 != (n = reader.read(buffer))) writer.write(buffer, 0, n);
            response_details = writer.toString();

            Log.d("@response", response_details);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void findDetails(String response) {
        try {
            noteSave.clear();

            JSONObject jsonObject = new JSONObject(response);

            cat.setText(jsonObject.getString("ProductCategory"));
            subCat.setText(jsonObject.getString("subCategory"));
            serial.setText("Serial No: " + jsonObject.getString("productSerial"));

            String issueS = jsonObject.getString("issue");
            issue.setText(issueS);

            String solution = jsonObject.getString("solution");
            Notes notes = new Notes(solution);
            notesAdapter.add(notes);
            noteSave.add(solution);

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("TEST_E", e.getMessage());
        }
    }

}
