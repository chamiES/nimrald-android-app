package com.nimrald.sevensigns.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.nimrald.sevensigns.myapplication.JobDetails_support.Previous_issues;
import com.nimrald.sevensigns.myapplication.JobDetails_support.Prevoiuse_issuesAdapter;
import com.nimrald.sevensigns.myapplication.ServiceFrag_support.Service;
import com.nimrald.sevensigns.myapplication.ServiceFrag_support.ServiceAdapter;
import com.nimrald.sevensigns.myapplication.URLs.URLs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class JobDetailActivity extends AppCompatActivity {
    String response_details;
    String response_serial;

    TextView companyName, customerName, date, corp, cat, subCat, serial, issue, tel, address;

    String id, serialNo;

    Prevoiuse_issuesAdapter prevoiuseIssuesAdapter;

    ListView preIssuListView;

    Button warranty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_detail);

        companyName = findViewById(R.id.CompanyName_JobDetails);
        customerName = findViewById(R.id.CustomeryName_JobDetails);
        date = findViewById(R.id.date_JobDetails);
        corp = findViewById(R.id.corp_JobDetails);
        cat = findViewById(R.id.category_JobDetails);
        subCat = findViewById(R.id.subCategory_JobDetails);
        serial = findViewById(R.id.serialNumber_JobDetails);
        issue = findViewById(R.id.issue_JobDetails);
        tel = findViewById(R.id.teleNo_JobDetails);
        address = findViewById(R.id.address_JobDetails);

        warranty = findViewById(R.id.warentyClaim_jobDetails);


        id = getIntent().getStringExtra("ID");
        serialNo = getIntent().getStringExtra("SER");


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            new JobDetailActivity.HTTPAsyncTaskJobDetails().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URLs.getIssuesURL() + "/" + id);
        } else {
            new JobDetailActivity.HTTPAsyncTaskJobDetails().execute(URLs.getIssuesURL() + "/" + id);
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            new JobDetailActivity.HTTPAsyncTaskPreviousIssues().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URLs.getIssuesURL());
        } else {
            new JobDetailActivity.HTTPAsyncTaskPreviousIssues().execute(URLs.getIssuesURL());
        }

        preIssuListView = findViewById(R.id.previousIssuesList);
        prevoiuseIssuesAdapter = new Prevoiuse_issuesAdapter(this, R.layout.job_details_custom_layout);
        preIssuListView.setAdapter(prevoiuseIssuesAdapter);

        warranty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(JobDetailActivity.this, WarrantyActivity.class);
                intent.putExtra("ID", id);
                startActivity(intent);
            }
        });

    }

    private class HTTPAsyncTaskJobDetails extends AsyncTask<String, Void, String> {

        ProgressDialog dialog = new ProgressDialog(JobDetailActivity.this);

        @Override
        protected void onPreExecute() {

            dialog.setMessage("Please wait....");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... urls) {
            try {
                try {
                    return HttpJobsRequest(urls[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                    return "Error!";
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();
            findDetails(response_details);
        }
    }

    private String HttpJobsRequest(String myUrl) throws IOException, JSONException {

        URL url = new URL(myUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
        conn.connect();

        InputStream inputStream = new BufferedInputStream(conn.getInputStream());
        convertStreamToString(inputStream);
        return conn.getResponseMessage() + "";
    }

    private void convertStreamToString(InputStream is) {
        try {
            response_details = null;
            int n = 0;
            char[] buffer = new char[1024 * 4];
            InputStreamReader reader = new InputStreamReader(is, "UTF8");
            StringWriter writer = new StringWriter();
            while (-1 != (n = reader.read(buffer))) writer.write(buffer, 0, n);
            response_details = writer.toString();

            Log.d("@response", response_details);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void findDetails(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);

            companyName.setText(jsonObject.getString("comapany"));
            customerName.setText(jsonObject.getString("customerName"));
            cat.setText(jsonObject.getString("ProductCategory"));
            subCat.setText(jsonObject.getString("subCategory"));
            tel.setText(jsonObject.getString("PhoneNumber"));
            date.setText(jsonObject.getString("Date"));
            serial.setText("Serial No: " + jsonObject.getString("productSerial"));

            String issueS = jsonObject.getString("issue");
            issue.setText(issueS);

            if (!issueS.contains("@System Generated"))
                corp.setVisibility(View.INVISIBLE);

            String a = jsonObject.getString("Address");
            String a1 = jsonObject.getString("Address1");
            String a2 = jsonObject.getString("Address2");
            String a3 = jsonObject.getString("Address3");

            String addre = "";
            if (a != null)
                addre = addre + a;

            if (a1 != null)
                addre = addre + ", " + a1;

            if (a2 != null)
                addre = addre + ", " + a2;

            if (a3 != null)
                addre = addre + ", " + a3;

            address.setText(addre);

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("TEST_E", e.getMessage());
        }
    }


    private class HTTPAsyncTaskPreviousIssues extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected String doInBackground(String... urls) {
            try {
                try {
                    return HttpPreIssuesRequest(urls[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                    return "Error!";
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            findPrevIssues(response_serial);
        }
    }


    private String HttpPreIssuesRequest(String myUrl) throws IOException, JSONException {

        URL url = new URL(myUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
        conn.connect();

        InputStream inputStream = new BufferedInputStream(conn.getInputStream());
        convertStreamToStringPreIssue(inputStream);
        return conn.getResponseMessage() + "";
    }

    private void convertStreamToStringPreIssue(InputStream is) {
        try {
            response_serial = null;
            int n = 0;
            char[] buffer = new char[1024 * 4];
            InputStreamReader reader = new InputStreamReader(is, "UTF8");
            StringWriter writer = new StringWriter();
            while (-1 != (n = reader.read(buffer))) writer.write(buffer, 0, n);
            response_serial = writer.toString();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void findPrevIssues(String response) {
        try {

            String issue, status, by;

            JSONArray JA = new JSONArray(response);

            for (int i = 0; i < JA.length(); i++) {
                JSONObject jsonObj = JA.getJSONObject(i);
                String srl = jsonObj.getString("productSerial");
                if (srl.equals(serialNo)) {
                    if (!id.equals(jsonObj.getString("id"))) {
                        issue = jsonObj.getString("issue");
                        status = jsonObj.getString("issueStatus");
                        by = jsonObj.getString("customerName");

                        Previous_issues previousIssues = new Previous_issues(issue, status, by);
                        prevoiuseIssuesAdapter.add(previousIssues);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("TEST_E", e.getMessage());
        }
    }

}
