package com.nimrald.sevensigns.myapplication.Note_support;

public class Notes {

    private String solution;

    public Notes(String solution){
        this.setSolution(solution);
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }
}
