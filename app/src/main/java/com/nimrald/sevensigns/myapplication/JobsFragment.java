package com.nimrald.sevensigns.myapplication;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.nimrald.sevensigns.myapplication.AlertDialogManager.AlertDialog;
import com.nimrald.sevensigns.myapplication.HaveNetwork.haveNetwork;
import com.nimrald.sevensigns.myapplication.JobsFrag_support.Jobs;
import com.nimrald.sevensigns.myapplication.JobsFrag_support.JobsAdapter;
import com.nimrald.sevensigns.myapplication.URLs.URLs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class JobsFragment extends Fragment {
    String response;
    JobsAdapter jobsAdapter;
    ListView jobListView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (haveNetwork.checkNetwork(getContext())) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new JobsFragment.HTTPAsyncTaskJobs().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URLs.getIssuesURL());
            } else {
                new JobsFragment.HTTPAsyncTaskJobs().execute(URLs.getIssuesURL());
            }
        } else {
            DialogInterface.OnClickListener okListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivityForResult(new Intent(Settings.ACTION_WIRELESS_SETTINGS), 0);
                }
            };

            AlertDialog.showNoInternetDialog(getContext(), okListener);
        }


        View view = inflater.inflate(R.layout.fragment_jobs, container, false);
        jobListView = view.findViewById(R.id.jobListView);
        jobsAdapter = new JobsAdapter(getContext(), R.layout.jobs_custom_layout);
        jobListView.setAdapter(jobsAdapter);

        jobListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (haveNetwork.checkNetwork(getContext())) {
                    TextView corp = view.findViewById(R.id.corp);
                    int visibility = corp.getVisibility();

                    TextView tx = view.findViewById(R.id.hiddenId);
                    String tId = tx.getText().toString();

                    TextView tx1 = view.findViewById(R.id.hiddenTele);
                    String tel = tx1.getText().toString();

                    TextView tx2 = view.findViewById(R.id.hiddenAddress);
                    String address = tx2.getText().toString();

                    TextView comp = view.findViewById(R.id.companyName);
                    String compName = comp.getText().toString();

                    TextView cust = view.findViewById(R.id.customerNme);
                    String custName = cust.getText().toString();

                    TextView date = view.findViewById(R.id.date);
                    String dateTx = date.getText().toString();

                    int pos = position + 1;

                    Intent intent = new Intent(getContext(), Jobs_sub.class);
                    intent.putExtra("ID", tId);
                    intent.putExtra("CORP", visibility);
                    intent.putExtra("TEL", tel);
                    intent.putExtra("ADD", address);
                    intent.putExtra("COM", compName);
                    intent.putExtra("CUS", custName);
                    intent.putExtra("DAT", dateTx);
                    intent.putExtra("NO", "Job No: "+pos);
                    startActivity(intent);

                } else {

                    DialogInterface.OnClickListener okListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivityForResult(new Intent(Settings.ACTION_WIRELESS_SETTINGS), 0);
                        }
                    };

                    AlertDialog.showNoInternetDialog(getContext(), okListener);
                }
            }
        });

        return view;
    }


    private class HTTPAsyncTaskJobs extends AsyncTask<String, Void, String> {

        ProgressDialog dialog = new ProgressDialog(getContext());

        @Override
        protected void onPreExecute() {

            dialog.setMessage("Please wait....");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... urls) {
            try {
                try {
                    return HttpJobsRequest(urls[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                    return "Error!";
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();
            findJobs(response);
        }
    }

    private String HttpJobsRequest(String myUrl) throws IOException, JSONException {

        URL url = new URL(myUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
        conn.connect();

        InputStream inputStream = new BufferedInputStream(conn.getInputStream());
        convertStreamToString(inputStream);
        return conn.getResponseMessage() + "";
    }

    private void convertStreamToString(InputStream is) {
        try {
            response = null;
            int n = 0;
            char[] buffer = new char[1024 * 4];
            InputStreamReader reader = new InputStreamReader(is, "UTF8");
            StringWriter writer = new StringWriter();
            while (-1 != (n = reader.read(buffer))) writer.write(buffer, 0, n);
            response = writer.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void findJobs(String response) {
        try {
            String id;
            JSONArray JA = new JSONArray(response);
            List<String> OpenjobID = new ArrayList<>();
            List<String> OpenJobCompanyname = new ArrayList<>();
            for (int i = 0; i < JA.length(); i++) {
                JSONObject jsonObj = JA.getJSONObject(i);
                String issueStatus = jsonObj.getString("issueStatus").trim();
                if (issueStatus.equals("Open")) {
                    id = jsonObj.getString("id");
                    OpenjobID.add(id);
                    OpenJobCompanyname.add(jsonObj.getString("comapany"));
                }
            }
            findSimilarJobs(response, OpenjobID, OpenJobCompanyname);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void findSimilarJobs(String response, List<String> IDs, List<String> comNames) {
        try {

            Set<String> UniqueCompanyNames = new HashSet<String>(comNames);
            List<String> UniqueCompanyNamesList = new ArrayList<>(UniqueCompanyNames);

            JSONArray JA = new JSONArray(response);

            for (int j = 0; j < UniqueCompanyNamesList.size(); j++) {
                String jobSearch = UniqueCompanyNamesList.get(j);

                String companyName = "N/A";
                String customerName = "N/A";
                String date = "N/A";
                String tel = "N/A";
                String address = "";
                String idString = "";
                String corp = "non";
                int similarIssueCount = 0;
                int similarServiceCount = 0;

                for (int i = 0; i < JA.length(); i++) {
                    JSONObject jsonObj = JA.getJSONObject(i);
                    if (jsonObj.getString("comapany").equals(jobSearch)) {
                        for (int t = 0; t < IDs.size(); t++) {
                            if (jsonObj.getString("id").equals(IDs.get(t))) {
                                address = "";
                                idString = idString + jsonObj.getString("id") + ", ";
                                companyName = jsonObj.getString("comapany");
                                customerName = jsonObj.getString("customerName");
                                date = jsonObj.getString("Date");
                                tel = jsonObj.getString("PhoneNumber");
                                String a1 = jsonObj.getString("Address");
                                String a2 = jsonObj.getString("Address1");
                                String a3 = jsonObj.getString("Address2");
                                String a4 = jsonObj.getString("Address3");

                                if (!a1.equals("NA"))
                                    address = address + a1;

                                if (!a2.equals("NA"))
                                    address = address + ", " + a2;

                                if (!a3.equals("NA"))
                                    address = address + ", " + a3;

                                if (!a4.equals("NA"))
                                    address = address + ", " + a4;

                                if (jsonObj.getString("issue").startsWith("@System Generated")) {
                                    corp = "corp";
                                    similarServiceCount++;
                                } else {
                                    similarIssueCount++;
                                }
                            }
                        }
                    }
                }

                String jobCountString = "";
                if (similarIssueCount == 0 && similarServiceCount > 0) {
                    jobCountString = similarServiceCount + " Services";
                }

                if (similarIssueCount > 0 && similarServiceCount == 0) {
                    jobCountString = similarIssueCount + " Issues";
                }

                if (similarIssueCount > 0 && similarServiceCount > 0) {
                    jobCountString = similarServiceCount + " Services | " + similarIssueCount + " Issues";
                }

                Jobs jobs = new Jobs(idString, companyName, customerName, date, jobCountString, corp, tel, address);
                jobsAdapter.add(jobs);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("@SIZE_e", e.getMessage());

        }
    }


}
