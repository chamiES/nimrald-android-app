package com.nimrald.sevensigns.myapplication.ServiceFrag_support;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.nimrald.sevensigns.myapplication.R;

import java.util.ArrayList;
import java.util.List;

public class ServiceAdapter extends ArrayAdapter {
    List list = new ArrayList();

    public ServiceAdapter(Context context, int resource) {
        super(context, resource);
    }

    public void add(Service object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row;
        row = convertView;
        ServiceAdapter.ServiceHolder serviceHolder;

        if (row == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.jobs_sub_custom_layout, parent, false);
            serviceHolder = new ServiceAdapter.ServiceHolder();
            serviceHolder.Category = row.findViewById(R.id.cat);
            serviceHolder.subCategory = row.findViewById(R.id.subCat);
            serviceHolder.Serial = row.findViewById(R.id.serialNo);
            serviceHolder.id = row.findViewById(R.id.hiddenId_job);
            row.setTag(serviceHolder);
        } else {
            serviceHolder = (ServiceAdapter.ServiceHolder) row.getTag();
        }

        Service service = (Service) this.getItem(position);

        serviceHolder.Category.setText(service.getCategory());
        serviceHolder.subCategory.setText(service.getSubCategory());
        serviceHolder.Serial.setText(service.getSerial());
        serviceHolder.id.setText(service.getId());

        return row;
    }

    static class ServiceHolder {
        TextView Category;
        TextView subCategory;
        TextView Serial;
        TextView id;
    }
}
