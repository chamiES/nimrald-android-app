package com.nimrald.sevensigns.myapplication;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.nimrald.sevensigns.myapplication.AlertDialogManager.AlertDialog;
import com.nimrald.sevensigns.myapplication.HaveNetwork.haveNetwork;
import com.nimrald.sevensigns.myapplication.IssueFrag_support.Issue;
import com.nimrald.sevensigns.myapplication.IssueFrag_support.IssueAdapter;
import com.nimrald.sevensigns.myapplication.ServiceFrag_support.Service;
import com.nimrald.sevensigns.myapplication.ServiceFrag_support.ServiceAdapter;
import com.nimrald.sevensigns.myapplication.URLs.URLs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class ServiceFragment extends Fragment {

    String response;
    ServiceAdapter serviceAdapter;
    ListView serviceListView;
    String ids;

    public ServiceFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Jobs_sub jobs_sub = (Jobs_sub) getActivity();
        ids = jobs_sub.getIDS();
        Log.d("@FRAG", ids);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            new HTTPAsyncTaskJobs().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URLs.getIssuesURL());
        } else {
            new HTTPAsyncTaskJobs().execute(URLs.getIssuesURL());
        }


        View view = inflater.inflate(R.layout.service_fragment, container, false);
        serviceListView = view.findViewById(R.id.serviceListView);
        serviceAdapter = new ServiceAdapter(getContext(), R.layout.jobs_sub_custom_layout);
        serviceListView.setAdapter(serviceAdapter);


        serviceListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (haveNetwork.checkNetwork(getContext())) {

                    TextView tx = view.findViewById(R.id.hiddenId_job);
                    String idH = tx.getText().toString();

                    TextView serialTx = view.findViewById(R.id.serialNo);
                    String serial = serialTx.getText().toString().substring(12);

                    Intent intent = new Intent(getContext(), JobDetailActivity.class);
                    intent.putExtra("ID",idH);
                    intent.putExtra("SER",serial);
                    startActivity(intent);

                } else {

                    DialogInterface.OnClickListener okListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivityForResult(new Intent(Settings.ACTION_WIRELESS_SETTINGS), 0);
                        }
                    };

                    AlertDialog.showNoInternetDialog(getContext(), okListener);
                }
            }
        });


        return view;
    }


    private class HTTPAsyncTaskJobs extends AsyncTask<String, Void, String> {

        ProgressDialog dialog = new ProgressDialog(getContext());

        @Override
        protected void onPreExecute() {

            dialog.setMessage("Please wait....");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... urls) {
            try {
                try {
                    return HttpJobsRequest(urls[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                    return "Error!";
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();
            findJobs(response);
        }
    }



    private String HttpJobsRequest(String myUrl) throws IOException, JSONException {

        URL url = new URL(myUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
        conn.connect();

        InputStream inputStream = new BufferedInputStream(conn.getInputStream());
        convertStreamToString(inputStream);
        return conn.getResponseMessage() + "";
    }

    private void convertStreamToString(InputStream is) {
        try {
            response = null;
            int n = 0;
            char[] buffer = new char[1024 * 4];
            InputStreamReader reader = new InputStreamReader(is, "UTF8");
            StringWriter writer = new StringWriter();
            while (-1 != (n = reader.read(buffer))) writer.write(buffer, 0, n);
            response = writer.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void findJobs(String response) {
        try {
            String[] idFrom = ids.split(",");
            String id;
            String cat;
            String subCat;
            String serial;
            JSONArray JA = new JSONArray(response);

            for (int i = 0; i < JA.length(); i++) {
                JSONObject jsonObj = JA.getJSONObject(i);
                String issueStatus = jsonObj.getString("issueStatus").trim();
                if (issueStatus.equals("Open")) {
                    for (String ids : idFrom) {
                        if (ids.trim().equals(jsonObj.getString("id").trim())) {
                            if (jsonObj.getString("issue").startsWith("@System Generated")) {
                                id = jsonObj.getString("id");
                                cat = jsonObj.getString("ProductCategory");
                                subCat = jsonObj.getString("subCategory");
                                serial = "Serial No : "+jsonObj.getString("productSerial");

                                Service service = new Service(cat, subCat, serial, id);
                                serviceAdapter.add(service);
                            }
                        }
                    }

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("TEST_E", e.getMessage());
        }
    }

}
