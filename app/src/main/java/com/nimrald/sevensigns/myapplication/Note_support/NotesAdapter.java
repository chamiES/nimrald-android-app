package com.nimrald.sevensigns.myapplication.Note_support;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.nimrald.sevensigns.myapplication.R;

import java.util.ArrayList;
import java.util.List;

public class NotesAdapter extends ArrayAdapter {

    List list = new ArrayList();

    public NotesAdapter(Context context, int resource) {
        super(context, resource);
    }


    public void add(Notes object) {
        super.add(object);
        list.add(object);
    }

    public void remove(int position){
        list.remove(position);
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row;
        row = convertView;
        NotesAdapter.NotesHolder notesHolder;

        if (row == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.note_custom_layout, parent, false);
            notesHolder = new NotesAdapter.NotesHolder();
            notesHolder.solution = row.findViewById(R.id.note);
            row.setTag(notesHolder);
        } else {
            notesHolder = (NotesAdapter.NotesHolder) row.getTag();
        }

        Notes notes = (Notes) this.getItem(position);
        notesHolder.solution.setText(notes.getSolution());
        return row;
    }

    static class NotesHolder {
        TextView solution;
    }
}
