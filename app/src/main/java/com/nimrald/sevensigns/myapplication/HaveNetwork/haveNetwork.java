package com.nimrald.sevensigns.myapplication.HaveNetwork;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class haveNetwork {

    public static boolean checkNetwork(Context context) {
        boolean haveNetwork = false;

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfos  = connectivityManager.getActiveNetworkInfo();

        if(networkInfos != null && networkInfos.isConnectedOrConnecting()){
            haveNetwork = true;
        }
        return haveNetwork;
    }
}
