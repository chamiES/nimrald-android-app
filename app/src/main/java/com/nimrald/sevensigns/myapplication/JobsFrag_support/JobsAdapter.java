package com.nimrald.sevensigns.myapplication.JobsFrag_support;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.nimrald.sevensigns.myapplication.R;

import java.util.ArrayList;
import java.util.List;

public class JobsAdapter extends ArrayAdapter {
    List list = new ArrayList();

    public JobsAdapter(Context context, int resource) {
        super(context, resource);
    }


    public void add(Jobs object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row;
        row = convertView;
        JobsHolder jobsHolder;

        if (row == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.jobs_custom_layout, parent, false);
            jobsHolder = new JobsHolder();
            jobsHolder.companyname = row.findViewById(R.id.companyName);
            jobsHolder.customerNme = row.findViewById(R.id.customerNme);
            jobsHolder.date = row.findViewById(R.id.date);
            jobsHolder.id = row.findViewById(R.id.hiddenId);
            jobsHolder.corp = row.findViewById(R.id.corp);
            jobsHolder.jobCount = row.findViewById(R.id.count);
            jobsHolder.Address = row.findViewById(R.id.hiddenAddress);
            jobsHolder.Tele = row.findViewById(R.id.hiddenTele);
            row.setTag(jobsHolder);
        } else {
            jobsHolder = (JobsHolder) row.getTag();
        }

        Jobs jobs = (Jobs) this.getItem(position);
        jobsHolder.companyname.setText(jobs.getCompanyName());
        jobsHolder.customerNme.setText(jobs.getCustomerName());
        jobsHolder.date.setText(jobs.getDate());
        jobsHolder.id.setText(jobs.getId());
        jobsHolder.jobCount.setText(jobs.getJobcount());
        jobsHolder.Tele.setText(jobs.getTele());
        jobsHolder.Address.setText(jobs.getAddress());

        if (jobs.getCorporate().equals("non")) {
            jobsHolder.corp.setVisibility(View.INVISIBLE);
        } else {
            jobsHolder.corp.setVisibility(View.VISIBLE);
        }
        return row;
    }

    static class JobsHolder {
        TextView companyname;
        TextView customerNme;
        TextView date;
        TextView id;
        TextView corp;
        TextView jobCount;
        TextView Address;
        TextView Tele;
    }
}
