package com.nimrald.sevensigns.myapplication;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.nimrald.sevensigns.myapplication.AlertDialogManager.AlertDialog;
import com.nimrald.sevensigns.myapplication.URLs.URLs;
import com.nimrald.sevensigns.myapplication.HaveNetwork.haveNetwork;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;



public class LoginActivity extends AppCompatActivity {

    private EditText userName;
    private EditText password;
    private Button loginButton;

    private String response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userName = findViewById(R.id.userName);
        password = findViewById(R.id.password);
        loginButton = findViewById(R.id.loginButton);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userName.getText().toString().trim().length() > 0 && password.getText().toString().trim().length() > 0) {

                    if (haveNetwork.checkNetwork(getApplicationContext())){
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            new HTTPAsyncTaskLogin().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URLs.getUserURL());
                        } else {
                            new HTTPAsyncTaskLogin().execute(URLs.getUserURL());
                        }
                    }else {
                        DialogInterface.OnClickListener okListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivityForResult(new Intent(Settings.ACTION_WIRELESS_SETTINGS), 0);
                            }
                        };

                        AlertDialog.showNoInternetDialog(LoginActivity.this,okListener);
                    }





                } else {
                    DialogInterface.OnClickListener okListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            userName.setText(null);
                            password.setText(null);
                        }
                    };

                    AlertDialog.showErrorAlert(LoginActivity.this, "All fields are needed.", okListener);
                }
            }
        });

    }


    private class HTTPAsyncTaskLogin extends AsyncTask<String, Void, String> {

        ProgressDialog dialog = new ProgressDialog(LoginActivity.this);

        @Override
        protected void onPreExecute() {

            dialog.setMessage("Please wait....");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... urls) {
            try {
                try {
                    return HttpLoginRequest(urls[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                    return "Error!";
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();
            doLogin(response);

        }
    }

    private String HttpLoginRequest(String myUrl) throws IOException, JSONException {

        URL url = new URL(myUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
        conn.connect();

        InputStream inputStream = new BufferedInputStream(conn.getInputStream());
        convertStreamToString(inputStream);
        return conn.getResponseMessage() + "";

    }

    private void convertStreamToString(InputStream is) {
        try {
            response = null;
            int n = 0;
            char[] buffer = new char[1024 * 4];
            InputStreamReader reader = new InputStreamReader(is, "UTF8");
            StringWriter writer = new StringWriter();
            while (-1 != (n = reader.read(buffer))) writer.write(buffer, 0, n);
            response = writer.toString();
            Log.d("@RESPONSE", response);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void doLogin(String response) {
        try {
            String userNameTxt = userName.getText().toString();
            String passwordtxt = password.getText().toString();
            JSONArray JA = new JSONArray(response);
            boolean found = false;
            for (int i = 0; i < JA.length(); i++) {
                JSONObject jsonObj = JA.getJSONObject(i);
                String userType = jsonObj.getString("userType").trim();
                if (userType.equals("Technician")) {
                    String userN = jsonObj.getString("userName").trim();
                    String pass = jsonObj.getString("password").trim();
                    if (userNameTxt.equals(userN) && passwordtxt.equals(pass)) {
                        found = true;
                    }
                }
            }
            if (found) {
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                DialogInterface.OnClickListener okListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                };
                AlertDialog.showErrorAlert(LoginActivity.this, "Invalid username or password", okListener);
                userName.setText(null);
                password.setText(null);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
