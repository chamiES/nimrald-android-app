package com.nimrald.sevensigns.myapplication.URLs;

public class URLs {

    public static String getUserURL(){
        return "http://5bde92fd7ad6890013e9c2b1.mockapi.io/user";
    }

    public static String getIssuesURL(){
        return "http://5bcd67b0a0c99b001337a8e0.mockapi.io/issues";
    }

}
