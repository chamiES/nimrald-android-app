package com.nimrald.sevensigns.myapplication.IssueFrag_support;

import android.util.Log;

public class Issue {
    private String category;
    private String subCategory;
    private String serial;
    private String id;

    public Issue(String category, String subCategory, String serial,String id) {
        this.setId(id);
        this.setCategory(category);
        this.setSubCategory(subCategory);
        this.setSerial(serial);
        Log.d("@TEST","jobCreated");
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
