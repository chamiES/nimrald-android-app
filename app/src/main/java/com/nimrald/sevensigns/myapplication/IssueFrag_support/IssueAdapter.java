package com.nimrald.sevensigns.myapplication.IssueFrag_support;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.nimrald.sevensigns.myapplication.JobsFrag_support.Jobs;
import com.nimrald.sevensigns.myapplication.JobsFrag_support.JobsAdapter;
import com.nimrald.sevensigns.myapplication.R;

import java.util.ArrayList;
import java.util.List;

public class IssueAdapter extends ArrayAdapter {
    List list = new ArrayList();

    public IssueAdapter(Context context, int resource) {
        super(context, resource);
    }

    public void add(Issue object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row;
        row = convertView;
        IssueAdapter.IssueHolder issueHolder;

        if (row == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.jobs_sub_custom_layout, parent, false);
            issueHolder = new IssueAdapter.IssueHolder();
            issueHolder.Category = row.findViewById(R.id.cat);
            issueHolder.subCategory = row.findViewById(R.id.subCat);
            issueHolder.Serial = row.findViewById(R.id.serialNo);
            issueHolder.id = row.findViewById(R.id.hiddenId_job);
            row.setTag(issueHolder);
        } else {
            issueHolder = (IssueAdapter.IssueHolder) row.getTag();
        }

        Issue issue = (Issue) this.getItem(position);

        issueHolder.Category.setText(issue.getCategory());
        issueHolder.subCategory.setText(issue.getSubCategory());
        issueHolder.Serial.setText(issue.getSerial());
        issueHolder.id.setText(issue.getId());

        return row;
    }

    static class IssueHolder {
        TextView Category;
        TextView subCategory;
        TextView Serial;
        TextView id;
    }
}
