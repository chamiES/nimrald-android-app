package com.nimrald.sevensigns.myapplication;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.nimrald.sevensigns.myapplication.JobsSub_support.ViewpagerAdapter;

public class Jobs_sub extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;

    private TextView corpText, companyName, customerName, jobNo, date, contactNo, address;
    private String ids;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jobs_sub);

        tabLayout = findViewById(R.id.tabLayoutId);
        viewPager = findViewById(R.id.viewPagerID);
        corpText = findViewById(R.id.corp_sub);
        companyName = findViewById(R.id.CompanyName_sub);
        customerName = findViewById(R.id.CustomeryName_sub);
        jobNo = findViewById(R.id.jobNo_sub);
        date = findViewById(R.id.date_sub);
        contactNo = findViewById(R.id.teleNo_sub);
        address = findViewById(R.id.address_sub);

        int corp = getIntent().getIntExtra("CORP", View.INVISIBLE);
        if (corp == View.INVISIBLE) {
            corpText.setVisibility(View.INVISIBLE);
        } else {
            corpText.setVisibility(View.VISIBLE);
        }
        ids = getIntent().getStringExtra("ID");

        companyName.setText(getIntent().getStringExtra("COM"));
        customerName.setText(getIntent().getStringExtra("CUS"));
        date.setText(getIntent().getStringExtra("DAT"));
        contactNo.setText(getIntent().getStringExtra("TEL"));
        address.setText(getIntent().getStringExtra("ADD"));
        jobNo.setText(getIntent().getStringExtra("NO"));

        ViewpagerAdapter viewpagerAdapter = new ViewpagerAdapter(getSupportFragmentManager());
        viewpagerAdapter.addFragment(new ServiceFragment(), "Services");
        viewpagerAdapter.addFragment(new IssuesFragment(), "Issues");

        viewPager.setAdapter(viewpagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText("Services");
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText("Issues");
        tabLayout.getTabAt(1).setCustomView(tabTwo);

    }


    public String getIDS() {
        return ids;
    }
}
