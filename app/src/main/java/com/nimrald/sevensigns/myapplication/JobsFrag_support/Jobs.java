package com.nimrald.sevensigns.myapplication.JobsFrag_support;

public class Jobs {
    private String companyName;
    private String customerName;
    private String date;
    private String jobcount;
    private String corporate;
    private String id;
    private String tele;
    private String address;

    public Jobs(String id, String companyName, String customerName, String date, String jobcount, String corporate, String tele, String address) {
        this.setId(id);
        this.setCompanyName(companyName);
        this.setCustomerName(customerName);
        this.setDate(date);
        this.setJobcount(jobcount);
        this.setCorporate(corporate);
        this.setTele(tele);
        this.setAddress(address);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getJobcount() {
        return jobcount;
    }

    public void setJobcount(String jobcount) {
        this.jobcount = jobcount;
    }

    public String getCorporate() {
        return corporate;
    }

    public void setCorporate(String corporate) {
        this.corporate = corporate;
    }

    public String getTele() {
        return tele;
    }

    public void setTele(String tele) {
        this.tele = tele;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
