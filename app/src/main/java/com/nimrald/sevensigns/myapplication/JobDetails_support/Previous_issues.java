package com.nimrald.sevensigns.myapplication.JobDetails_support;

public class Previous_issues {

    private String issue;
    private String status;
    private String by;

    public Previous_issues(String issue, String status, String by) {
        this.setIssue(issue);
        this.setStatus(status);
        this.setBy(by);
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
    }
}
