package com.nimrald.sevensigns.myapplication.JobDetails_support;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.nimrald.sevensigns.myapplication.R;

import java.util.ArrayList;
import java.util.List;

public class Prevoiuse_issuesAdapter extends ArrayAdapter {

    List list = new ArrayList();

    public Prevoiuse_issuesAdapter(Context context, int resource) {
        super(context, resource);
    }
    
    public void add(Previous_issues object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row;
        row = convertView;
        PreviousIssuesHolder previousIssuesHolder;

        if (row == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.job_details_custom_layout, parent, false);
            previousIssuesHolder = new PreviousIssuesHolder();

            previousIssuesHolder.issue = row.findViewById(R.id.issue_JobDetails_custom);
            previousIssuesHolder.status = row.findViewById(R.id.status_JobDetails_custom);
            previousIssuesHolder.by = row.findViewById(R.id.by_JobDetails_custom);
            row.setTag(previousIssuesHolder);

        } else {
            previousIssuesHolder = (PreviousIssuesHolder) row.getTag();
        }

        Previous_issues previousIssues = (Previous_issues) this.getItem(position);
        previousIssuesHolder.issue.setText(previousIssues.getIssue());
        previousIssuesHolder.status.setText(previousIssues.getStatus());
        previousIssuesHolder.by.setText("by "+previousIssues.getBy());

        return row;
    }

    static class PreviousIssuesHolder {
        TextView issue;
        TextView status;
        TextView by;
    }
    
}
